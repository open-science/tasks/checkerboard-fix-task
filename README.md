This flashing checkerboard script presents black screen vs flashing checkerboards in a block design. You will need a button box / gamepad device to trigger and record responses. Participant fixation is controlled via a simple fixation task. Tried and tested on the 7T FMRIB.

Coded in Psychtoolbox. Please contact Betina Ip (betina.ip@dpag.ox.ac.uk) if you have any questions about the code.