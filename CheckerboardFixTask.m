%% CheckerboardFixTask.m
%
% Flashing radial checkerboards in block design with fixation task
% You will need a Gamepad to test responses
%
% Log
% 
% Written by Betina Ip 30/07/15
% Added recordin g of button presses DONE 3/8/15
% Added evaluation of responses DONE 3/8/15
% Added subject facing percelhnt correct DONE 3/8/15
% Added double check response code DONE 4/8/15
% Added subject and stimulus output file  4/8/15
% Added subject facing counter-balanced left/right eye instruction
% Added minimum interval between target events 12/8/2015
% Added taking input from all buttons 4/9/2015
% Added structure containing order of conditions shown

%Betina Ip
%DPAG, University of Oxford

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set up workspace
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all

%addpath /Users/betinaip/matlab/checkerboard/

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %Set up output file details
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% exp.date=date;
% exp.runOrder={'Right','Both','Left'};%counterbalancing order
% exp.name={'RW'};
% bgcolor=[0 0 0]; %black OFF period
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %Set up experiment parameters (repetitions, duration etc)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %4 repetitions : this runs for 512 s
% rep=4;%repetitions of an On-Off cycle, main =4, water=2
% duration=64;%duration of block in sec, TR=4, 16 volumes/block
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set up if trigger will be from scanner or manual, 
%the block duration
%the number of ON-OFF cycles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

duration=61;%duration of block in sec
rep=2;%repetitions of an On-Off cycle , always starts with a off block
wait2trigger=1;%this parameter sets how long after the trigger the program waits until it starts the visual stimulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set up output file details
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

exp.date=date;
exp.runOrder={'Right','Both','Left'};%counterbalancing order
exp.name={'My_Pilot'};
bgcolor=[0 0 0]; %black OFF period

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set up screen parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Screen('Preference', 'SkipSyncTests', 1);
Screen('Preference', 'VisualDebugLevel', 0);
myScreen = max(Screen('Screens'));%picks the secondary monitor if connected, else primary monitor
[win,winRect] = Screen(myScreen,'OpenWindow');
[width, height] = RectSize(winRect);
display.resolution = [width, height];
display.width = 52;
display.dist = 60;  %viewing distance in cm
display.windowPtr= win;
display.frameRate = 1/Screen('GetFlipInterval',win); %Hz
ifi = Screen('GetFlipInterval', win);
frequency=8;%flicker frequency
swapinterval = 1 / frequency;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Make radial checkerboard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[xc,yc] = RectCenter(winRect);
rcycles = 4; % number of white/black circle pairs
tcycles = 8; % number of white/black angular segment pairs (integer)
xc = winRect(3)/2;
yc = winRect(4)/2;
hi_index=(255/2)+(255/2)/2;%set your color and contrast here
lo_index=(255/2)-(255/2)/2;%set your color and contrast here
bg_index=0;
bg_index =[255/2 255/2 255/2] ;%set the baseline color here

xysize = winRect(4);
xysize = 2*(winRect(4));
s = xysize/sqrt(2); % size used for mask
xylim = 2*pi*rcycles;
[x,y] = meshgrid(-xylim:2*xylim/(xysize-1):xylim, - ...
    xylim:2*xylim/(xysize-1):xylim);
at = atan2(y,x);
checks = ((1+sign(sin(at*tcycles)+eps) .* ...
    sign(sin(sqrt(x.^2+y.^2))))/2) * (hi_index-lo_index) + lo_index;
texture(1) = Screen('MakeTexture', win, checks);
checks2=checks;
checks2(find(checks==hi_index))=lo_index;
checks2(find(checks==lo_index))=hi_index;
%texture(2) = Screen('MakeTexture', win, hi_index - checks); % reversed contrast
texture(2) = Screen('MakeTexture', win, checks2); % reversed contrast


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%parameters of fixation target and task
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nChanges=round(duration*2*rep/3);%event occur about every 3 seconds
r=sort(randperm(duration*2*rep,nChanges));
event=1;%sets the contrast change counter to 1
r=[r,999];
targetdur=0.5;%in seconds
interval=2;%minimum separation between events
for i =1:size(r,2)-1;%set minimum separate interval
    if r(i)>r(i+1)-interval;%if smaller than min separation
        r(i+1)=r(i)+interval ;%increase by min separation
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set up button recording using gamepad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clear gamepad entries
Gamepad('Unplug');

% Find last gamepad plugged in
NumDevices = Gamepad('GetNumGamepads');
if NumDevices == 0
    error('No gamepad detected')
else
    ButtonBox = NumDevices;
end

%how many buttons?
Buttons = Gamepad('GetNumButtons', ButtonBox);
DispTime = 0;
rt=1;
RespTime=[];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Wait for trigger
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Screen('TextSize',win,35)
msg=['The experiment is about to start.\n Please fixate on the central square \nand press the button  as quick as you can when it turns red.']
Screen('FillRect',win)
DrawFormattedText(win,msg,'center','center',[0 0 0]);
Screen('Flip', display.windowPtr );


% while KbCheck;
% end
% KbWait

while 1;
    %[x,y,buttonState] = GetMouse;% use for simulated trigger input
    
    buttonState = Gamepad('GetButton', 1, 5);% use for MRI
    %     trigger
    if (buttonState)==0;
        fprintf('waiting...\n')
    else
        (buttonState)~=0;
        timing=GetSecs;
        fprintf('Trigger received\n')
        break 
    end
end

WaitSecs(wait2trigger)% waits for duration of dummy trigger (2 x 4 sec)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Experiment starts here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

expstart=GetSecs;
% Screen('DrawTexture',win,texture(1)); %add position
% Screen('DrawTexture',win,texture(2)); %add position
display.fixation.flip=0;% First flip to get the time code we use in the loop
Screen('FillRect',win,[128 128 128]);
drawfixation_v2(display,0.5,{[255,255,255],[255,255,255]});% First flip to get the time code we use in the loop
Screen('Flip',win);
Screen('FillRect',win,[128 128 128]);
drawfixation_v2(display,0.5,{[255,255,255],[255,255,255]});% First flip to get the time code we use in the loop
VBLTimestamp = Screen('Flip',win, 0, 2);%flip to second window

tic


for i = 1:rep;
    %Start of the blank period, same duration as stimulus period
    deadline=GetSecs+duration;
    while (GetSecs < deadline);
        %        ButtonState = Gamepad('GetButton', ButtonBox, 1);
        for b = 1:Buttons
            ButtonState(b) = Gamepad('GetButton', ButtonBox, b);
        end
        Screen('FillRect',win,bgcolor);
        if (GetSecs-expstart > r(event)) && (GetSecs-expstart < r(event)+targetdur) ; %change in value
            display.fixation.color={[255,0,0],[255,0,0]};
        else
            display.fixation.color={[255,255,255],[255,255,255]};
        end
        if sum(ButtonState) > 0
            disp(['Pressed:' num2str(find(ButtonState))])
            DispTime = GetSecs;
            RespTime(rt)=DispTime-expstart
            rt=rt+1;
        end
        if GetSecs-expstart > r(event)+targetdur
            event=event+1;
        end
        drawfixation_v2(display,0.5,display.fixation.color);% First flip to get the time code we use in the loop
        [VBLTimestamp, StimulusOnseTime] = Screen('Flip',win, VBLTimestamp + swapinterval,2);
    end
    Screen('DrawTexture',win,texture(1)); %add position
    Screen('Flip',win);
    Screen('DrawTexture',win,texture(2)); %add position
    drawfixation_v2(display,0.5,display.fixation.color);
    VBLTimestamp = Screen('Flip',win, 0, 2);
    deadline=GetSecs+duration;
    while (GetSecs < deadline)
        %                 ButtonState = Gamepad('GetButton', ButtonBox, 2);
        for b = 1:Buttons
            ButtonState(b)= Gamepad('GetButton', ButtonBox, b);
        end
         if (GetSecs-expstart > r(event)) && (GetSecs-expstart < r(event)+targetdur) ; %change in value
            display.fixation.color={[255,0,0],[255,0,0]};
        else
            display.fixation.color={[255,255,255],[255,255,255]};
         end
            
         if sum(ButtonState) > 0
            disp(['Pressed:' num2str(find(ButtonState))])
            DispTime = GetSecs;
            RespTime(rt)=DispTime-expstart
            rt=rt+1;
        end
        if GetSecs-expstart > r(event)+targetdur
            event=event+1;
        end
        drawfixation_v2(display,0.5,display.fixation.color);% First flip to get the time code we use in the loop
        [VBLTimestamp, StimulusOnseTime] = Screen('Flip',win, VBLTimestamp + swapinterval,2);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %evaluate responses
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    output=zeros(size(r,2),3);
    for i=1:size(r,2)
        if (RespTime(r(i)+0.150 < RespTime(:) & r(i)+2 > RespTime(:)))~=0
            press=RespTime(r(i)+0.050 < RespTime(:) & r(i)+2 > RespTime(:));%within time interval (+0.1-2 sec
            mean(press-r(i));%mean response time for that event
            output(i,:)=[r(i) 1 mean(press-r(i))] ;%[targetonset response(Yes/No) meanRT]
        else
            output(i,:)=[r(i) 0 NaN];%if no response matching, note none given
        end
    end
end
toc

output(end,:)=[];%remove the final row
pc=(sum(output(:,2)))./size(output,1)*100
rtime=nanmean(output(:,3))
Gamepad('Unplug');

c=clock;
c=fix(c);
num2str([c(3),c(2),c(1),c(4),c(5)]);

dlmwrite(['/Users/betinaip/matlab/mrsepi/Checkerboard_',date,'_',num2str(c(4)),num2str(c(5)),'.txt'],output,'delimiter','\t')
save(['/Users/betinaip/matlab/mrsepi/Checkerboard_exp','_',date,'_',num2str(c(4)),num2str(c(5)),'.mat'],'-struct','exp')
fprintf('files saved...')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Volunteer message
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Volunteer detected %4.2f percent of the targets. The mean reaction time was %4.2f sec.',pc,rtime)
msg=['You detected ',num2str(pc),'% of the targets.\n  Your mean reaction time was ',num2str(rtime),'sec.']
Screen('FillRect',win)
DrawFormattedText(win,msg,'center','center',[0 0 0]);
Screen('Flip', display.windowPtr );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cleaning up workspace
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sca
return
fprintf('Experiment finished.')

% Done